#ifndef MYTIME_H
#define MYTIME_H

class QString;

class MyOwnTime
{
public:
    MyOwnTime();
    MyOwnTime(int hours, int minutes);
    MyOwnTime &operator +(const int &mins);
    int operator-(const MyOwnTime &time);
    bool valid();
    QString convertToQString();
    void SetTime(int hrs, int mins);

private:
    int hours, minutes;
};

#endif // MYTIME_H

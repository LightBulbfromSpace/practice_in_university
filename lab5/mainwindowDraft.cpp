#include "mainwindowDraft.h"
#include "ui_mainwindow.h"
#include "design.cpp"
#include <QLineEdit>
#include <QIntValidator>
#include <QTime>
#include <QTimer>
#include <QLabel>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    tmr = new QTimer(this);
    connect(tmr, SIGNAL(timeout()), this, SLOT(showTime()));

    quit = new QPushButton(this);
    connect(quit, SIGNAL(clicked()), this, SLOT(close()));
    DesignForQuit(quit);

    curr_time_label = new QLabel(this);
    curr_time_label->hide();
    DesignForCurr_time_label(curr_time_label);

    curr_time = new QLabel(this);
    curr_time->hide();
    DesignForCurr_time(curr_time);

    edit_common = new QLineEdit(this);
    edit_common->hide();

    edit_h = new QLineEdit(this);
    edit_h->hide();

    edit_m = new QLineEdit(this);
    edit_m->hide();

    back = new QPushButton(this);
    DesignForBack(back);
    connect(back, SIGNAL(clicked()), this, SLOT(on_Back_clicked()));
    back->hide();

    result = new QPushButton(this);
    DesignForGetResult(result);
    connect(result, SIGNAL(clicked()), this, SLOT(on_GetResult_clicked()));
    result->hide();

    val = new QIntValidator(this);
}

MainWindow::~MainWindow()
{
    delete ui;
    delete curr_time;
    delete result;
    delete my_time;
    delete edit_h;
    delete edit_m;
    delete edit_common;
    delete back;
    delete tmr;
    delete quit;
    delete val;
}


void MainWindow::on_Minus_clicked()
{
    ui->Minus->hide();
    ui->Plus->hide();
    quit->hide();

    tmr->start(1000);
    curr_time_label->show();
    curr_time->show();

    if(!your_time_label)
        your_time_label = new QLabel(this);
    DesignForYour_time_label(your_time_label);
    your_time_label->show();

    DesignForEdit_h(edit_h);
    edit_h->setValidator(val);
    edit_h->show();
    DesignForEdit_m(edit_m);
    edit_m->setValidator(val);
    edit_m->show();

    result->show();
    back->show();

    flag = false;
}


void MainWindow::on_Plus_clicked()
{
    ui->Minus->hide();
    ui->Plus->hide();
    quit->hide();

    tmr->start(1000);
    curr_time_label->show();
    curr_time->show();

    if(!your_minutes_label)
        your_minutes_label = new QLabel(this);
    DesignForYour_minutes_label(your_minutes_label);
    your_minutes_label->show();

    DesignForEdit_common(edit_common);
    edit_common->setValidator(val);
    edit_common->show();

    result->show();
    back->show();

    flag = true;

}

void MainWindow::on_GetResult_clicked()
{

    QMessageBox msg;
    DesignForMsgBox(&msg);
    if (flag)
    {

        int mins = edit_common->text().toInt();
        qDebug("%d", mins);
        MyOwnTime time_now(QTime::currentTime().hour(), QTime::currentTime().minute());
        MyOwnTime result = time_now + mins;
        msg.setText(result.convertToQString());
        msg.exec();
    }
    else
    {
        MyOwnTime *my_time = new MyOwnTime;
        my_time->SetTime(edit_h->text().toInt(), edit_m->text().toInt());
         if (my_time->valid())
         {
            MyOwnTime time_now(QTime::currentTime().hour(), QTime::currentTime().minute());
            int difference;
            difference = time_now - *my_time;
            QString string;
            string.setNum(difference, 10);
            msg.setText(string);
            msg.exec();
        }
        else
        {
            msg.setText("Error");
            msg.exec();
        }
         delete my_time;
    }
}

void MainWindow::on_Back_clicked()
{
    ui->Minus->show();
    ui->Plus->show();
    quit->show();
    curr_time->hide();
    curr_time_label->hide();
    tmr->stop();
    result->hide();
    edit_h->hide();
    edit_m->hide();
    edit_common->hide();
    back->hide();
    if (your_time_label)
    {
        your_time_label->hide();
        your_time_label = nullptr;
    }
    if (your_minutes_label)
    {
        your_minutes_label->hide();
        your_minutes_label = nullptr;
    }
}

void MainWindow::showTime()
{
    curr_time->show();
    curr_time->setText(QTime::currentTime().toString("h:mm:ss "));
}

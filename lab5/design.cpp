#include <QTime>
#include <QMessageBox>

void DesignForCurr_time_label(QLabel *curr_time_label)
{
    curr_time_label->resize(200,40);
    curr_time_label->move(100,20);
    curr_time_label->setStyleSheet( "color: #cccccc;"
                                    "font-family: Courier New, monospace;"
                                    "font-size: 16px;");
    curr_time_label->setAlignment(Qt::AlignCenter);
    curr_time_label->setText("Current time:");
}

void DesignForCurr_time(QLabel *curr_time)
{
    curr_time->setStyleSheet(	"border-radius: 10px;"
                                "border-style: solid;"
                                "border-width: 2px;"
                                "border-color: #2ba68b;"
                                "background-color: #545454;"
                                "color: #cccccc;"
                                "font-family: Courier New, monospace;"
                                "font-size: 16px;");
    curr_time->setAlignment(Qt::AlignCenter);
    curr_time->resize(200,40);
    curr_time->move(100,70);
    curr_time->setText(QTime::currentTime().toString("h:mm:ss "));
}

void DesignForYour_time_label(QLabel *your_time_label)
{
    your_time_label->resize(200,40);
    your_time_label->move(100,120);
    your_time_label->setStyleSheet( "color: #cccccc;"
                                    "font-family: Courier New, monospace;"
                                    "font-size: 16px;");
   your_time_label->setAlignment(Qt::AlignCenter);
   your_time_label->setText("Write your time:");
}

void DesignForEdit_h(QLineEdit *edit_h)
{
    edit_h->setPlaceholderText("00");
    edit_h->resize(80,40);
    edit_h->move(110,170);
    edit_h->setStyleSheet(	"border-radius: 10px;"
                            "border-style: solid;"
                            "border-width: 2px;"
                            "border-color: #2ba68b;"
                            "background-color: #dedede;"
                            "color: #919191;"
                            "font-family: Courier New, monospace;"
                            "font-size: 16px;"
                            "font-weight: bold;"
                            "selection-background-color: #bababa;");
    edit_h->setAlignment(Qt::AlignCenter);
}

void DesignForEdit_m(QLineEdit *edit_m)
{
    edit_m->setPlaceholderText("00");
    edit_m->resize(80,40);
    edit_m->move(210,170);
    edit_m->setStyleSheet( "border-radius: 10px;"
                           "border-style: solid;"
                           "border-width: 2px;"
                           "border-color: #2ba68b;"
                           "background-color: #dedede;"
                           "color: #919191;"
                           "font-family: Courier New, monospace;"
                           "font-size: 16px;"
                           "font-weight: bold;"
                           "selection-background-color: #bababa;");
   edit_m->setAlignment(Qt::AlignCenter);
}

void DesignForGetResult(QPushButton *result)
{
    result->resize(200,40);
    result->move(100,230);
    result->setText("Get Result");
    result->setStyleSheet(	"border-radius: 10px;"
                            "border-style: solid;"
                            "border-width: 2px;"
                            "border-color: #2ba68b;"
                            "background-color: #4d4d4d;"
                            "color: #d4d4d4;"
                            "font-family: Courier New, monospace;"
                            "font-size: 16px;"
                            "font-weight: bold;"
                            "selection-background-color: #bababa;");
}

void DesignForBack(QPushButton* back)
{
    back->resize(50,40);
    back->move(15,15);
    back->setText("Back");
    back->setStyleSheet(	"border-radius: 10px;"
                            "border-style: solid;"
                            "border-width: 2px;"
                            "border-color: #1b6e5b;"
                            "background-color: #303030;"
                            "color: #d4d4d4;"
                            "font-family: Courier New, monospace;"
                            "font-size: 16px;"
                            "selection-background-color: #bababa;");
}

void DesignForYour_minutes_label(QLabel *your_minutes_label)
{
    your_minutes_label->resize(200,40);
    your_minutes_label->move(100,120);
    your_minutes_label->setStyleSheet(  "color: #cccccc;"
                                        "font-family: Courier New, monospace;"
                                        "font-size: 16px;");
   your_minutes_label->setAlignment(Qt::AlignCenter);
   your_minutes_label->setText("Number of minutes:");
}

void DesignForEdit_common(QLineEdit *edit_common)
{
    edit_common->resize(200,40);
    edit_common->move(100,170);
    edit_common->setMaxLength(4);
    edit_common->setPlaceholderText("0000");
    edit_common->setStyleSheet(	"border-radius: 10px;"
                                "border-style: solid;"
                                "border-width: 2px;"
                                "border-color: #2ba68b;"
                                "background-color: #dedede;"
                                "color: #919191;"
                                "font-family: Courier New, monospace;"
                                "font-size: 16px;"
                                "font-weight: bold;"
                                "selection-background-color: #bababa;");
    edit_common->setAlignment(Qt::AlignCenter);
}

void DesignForMsgBox(QMessageBox *msg)
{
    msg->setWindowTitle("Result");
    msg->setStyleSheet("QMessageBox"
                       "{"
                       "background-color: black;"
                       "}"
                       "QLabel"
                       "{"
                       "background-color: black;"
                       "font-family: Courier New, monospace;"
                       "font-size: 20px;"
                       "color: #d4d4d4;"
                       "font-weight: bold;"
                       "min-width: 70px;"
                       "min-height: 30px;"
                       "}"
                       "QPushButton"
                       "{"
                       "background-color: gray;"
                       "border-radius: 10px;"
                       "border-style: solid;"
                       "border-width: 2px;"
                       "border-color: #2ba68b;"
                       "min-width: 80px;"
                       "min-height: 20px;"
                       "text-align: center;"
                       "font-family: Courier New, monospace;"
                       "font-size: 18px;"
                       "color: #d4d4d4;"
                       "font-weight: bold;"
                       "}");

}

void DesignForQuit(QPushButton *quit)
{
    quit->setStyleSheet( "border-radius: 10px;"
                         "border-style: solid;"
                         "border-width: 2px;"
                         "border-color: #2ba68b;"
                         "background-color: #545454;"
                         "color: #cccccc;"
                         "font-family: Courier New, monospace;"
                         "font-size: 16px;");
    quit->setText("Quit");
    quit->resize(70,37);
    quit->move(160,210);
}

#include "myowntime.h"
#include <cmath>
#include <QDebug>

MyOwnTime::MyOwnTime()
{

}

MyOwnTime::MyOwnTime(int hours, int minutes)
{
    this->hours = hours % 24;
    this->minutes = minutes % 60;
}

MyOwnTime& MyOwnTime::operator +(const int &mins)
{

    hours = (hours + mins/60) % 24;
    minutes = (minutes + mins) % 60;
    return *this;
}

int MyOwnTime::operator -(const MyOwnTime &time)
{
    return abs(this->hours - time.hours)*60 + abs(this->minutes - time.minutes);
}

bool MyOwnTime::valid()
{
    return (hours < 24 && hours >=0 && minutes < 60 && minutes >=0);
}

QString MyOwnTime::convertToQString()
{
    QString string1;
    QString string2;
    string1.setNum(hours, 10);
    if (minutes > 9)
        string1 += ":" + string2.setNum(minutes, 10);
    else
        string1 += ":0" + string2.setNum(minutes, 10);
    return string1;
}

void MyOwnTime::SetTime(int hrs, int mins)
{
    hours = hrs;
    minutes = mins;
}

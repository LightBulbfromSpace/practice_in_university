#ifndef MAINWINDOWDRAFT_H
#define MAINWINDOWDRAFT_H

#include <QMainWindow>
#include <QLabel>
#include <QPushButton>
#include <QLineEdit>
#include <QTimer>
#include "myowntime.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_Minus_clicked();
    void on_Plus_clicked();
    void on_GetResult_clicked();
    void on_Back_clicked();
//    void on_Quit_clicked();
    void showTime();

private:
    Ui::MainWindow *ui;
    QPushButton *quit = nullptr;
    QLabel *curr_time = nullptr; // hh:mm:ss (for minus and plus)
    QPushButton *result = nullptr; //GetResult
    MyOwnTime *my_time = nullptr;
    QLineEdit *edit_h = nullptr;
    QLineEdit *edit_m = nullptr;
    QLineEdit *edit_common = nullptr;
    QPushButton *back = nullptr;
    QTimer *tmr = nullptr;
    QLabel *your_time_label = nullptr;
    QLabel *your_minutes_label = nullptr;
    QLabel *curr_time_label = nullptr; //Current time: (for minus and plus)
    QValidator *val;

    bool flag;
};
#endif // MAINWINDOWDRAFT_H

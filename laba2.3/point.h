#ifndef POINT_H
#define POINT_H

class QPainter;

class Point
{
public:
    Point();

    void setCoords(int x, int y);
    void drawCross(QPainter *painter);
    void setOffset (int offset);
    void setWidth(int width);

private:
    int p_x, p_y, p_offset = 2, p_width = 1;
};

#endif // POINT_H

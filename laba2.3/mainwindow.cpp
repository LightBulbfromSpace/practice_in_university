#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMouseEvent>
#include <QPaintEvent>
#include <QPainter>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    points = new Point[10];
}

MainWindow::~MainWindow()
{
    delete ui;
    delete[] points;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    if (points)
    {
        for (int j = 0; j < i; ++j)
            points[j].drawCross(&painter);
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
        if (i < 10)
        {
            p.setCoords(event->pos().x(), event->pos().y());
            p.setOffset(1);
            if (i > 4)
                p.setWidth(2);
            points[i] = p;
            ++i;
        }
    update();
}


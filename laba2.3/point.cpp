#include "point.h"
#include <cmath>
#include <QPainter>

Point::Point()
{

}

void Point::setCoords(int x, int y)
{
    p_x = x;
    p_y = y;
}

void Point::drawCross(QPainter *painter)
{
    QPen pen;
    pen.setWidth(p_width);
    painter->setPen(pen);
    painter->drawLine(p_x - p_offset, p_y, p_x + p_offset, p_y);
    painter->drawLine(p_x, p_y - p_offset, p_x, p_y + p_offset);
}

void Point::setOffset(int offset)
{
    p_offset = offset;
}

void Point::setWidth(int width)
{
    p_width = width;
}

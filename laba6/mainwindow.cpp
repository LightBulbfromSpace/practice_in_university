#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "rect.h"
#include <QMouseEvent>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    Palette(&painter);
    for (int i = 0; i < intersections.size(); ++i) // order of this two cycles influences "type of intersection"
        intersections[i].Draw(&painter);           // (this order matches "intersection" and inverse one matches "clip")
    for (int i = 0; i < rects.size(); ++i)
        rects[i].Draw(&painter);
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    static int x1 = 0, y1 = 0;
    if (event->modifiers() & Qt::ControlModifier)
    {
        if ((x1 && y1) == 0)
        {
            x1 = event->pos().x();
            y1 = event->pos().y();
        }
        else
        {
            Rect rect(x1, y1, event->pos().x(), event->pos().y());
            rects.push_back(rect);
            x1 = 0;
            y1 = 0;
            update();
        }
    }
    else
    {
        Rect inters;
        QPoint point;
        int count = 1;
        point.setX(event->pos().x());
        point.setY(event->pos().y());
        for (int i = 0; i < rects.size(); ++i)
            if (rects[i].Contains(&point))
            {
                inters.Intersection(&rects[i]);
                inters.setCount(count++);
            }
        if (count > 1)
            intersections.push_back(inters);
        update();
    }
}


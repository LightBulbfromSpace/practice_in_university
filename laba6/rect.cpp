#include "rect.h"
#include <QPainter>
#include <QBrush>
#include <QColor>

Rect::Rect()
{

}

Rect::Rect(int x1, int y1, int x2, int y2)
{
    if (x1 > x2)
        std::swap(x1, x2);
    if (y1 > y2)
        std::swap(y1, y2);
    r_x1 = x1;
    r_y1 = y1;
    r_x2 = x2;
    r_y2 = y2;
}

void Rect::Draw(QPainter *painter)
{
    switch (count) {
    case 1:
        painter->setBrush(QColor(0, 0, 0, 0));
        break;
    case 2:
        painter->setBrush(QColor(255, 255, 0, 128));
        break;
    case 3:
        painter->setBrush(QColor(255, 0, 255, 128));
        break;
    case 4:
        painter->setBrush(QColor(0, 255, 255, 128));
        break;
    case 5:
        painter->setBrush(QColor(0, 0, 128, 128));
        break;
    default:
        painter->setBrush(QColor(0, 0, 0, 128));
        break;
    }
    painter->drawRect(r_x1, r_y1, r_x2 - r_x1, r_y2 - r_y1);
}

void Rect::setCoords(int x1, int y1, int x2, int y2)
{
    if (x1 > x2)
        std::swap(x1, x2);
    if (y1 > y2)
        std::swap(y1, y2);
    r_x1 = x1;
    r_y1 = y1;
    r_x2 = x2;
    r_y2 = y2;
}

void Rect::setPoint1(int x, int y)
{
    r_x1 = x;
    r_y1 = y;
}

void Rect::setPoint2(int x, int y)
{
    r_x2 = x;
    r_y2 = y;
}

void Rect::Intersection(Rect *rect)
{
    if (r_x1 < rect->r_x1 || r_x1 == 0)
        r_x1 = rect->r_x1;
    if (r_x2 > rect->r_x2 || r_x2 == 0)
        r_x2 = rect->r_x2;
    if (r_y1 < rect->r_y1 || r_y1 == 0)
        r_y1 = rect->r_y1;
    if (r_y2 > rect->r_y2 || r_y2 == 0)
        r_y2 = rect->r_y2;
}

bool Rect::Contains(QPoint *point)
{
    return ((r_x1 - point->x())*(r_x2 - point->x()) < 0)
            && ((r_y1 - point->y())*(r_y2 - point->y()) < 0);
}

void Rect::setCount(int newCount)
{
    count = newCount;
}

void Palette(QPainter *painter)
{
    for (int i = 2; i < 7; ++i)
    {
        Rect rect;
        rect.setCount(i);
        rect.setPoint1((i-2)*20 + 1, 1);
        rect.setPoint2((i-1)*20 + 1, 20);
        rect.Draw(painter);
        QString string;
        string.setNum(i, 10);
        painter->drawText((i-2)*20 + 8, 35, string);
    }

}

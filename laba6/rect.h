#ifndef RECT_H
#define RECT_H

class QPainter;
class QPoint;

class Rect
{
public:
    Rect();
    Rect(int x1, int y1, int x2, int y2);
    void Draw(QPainter *painter);
    void setCoords(int x1, int y1, int x2, int y2);
    void setPoint1(int x, int y);
    void setPoint2(int x, int y);
    void Intersection(Rect *rect);
    bool Contains(QPoint *point);
    void setCount(int newCount);

private:
    int r_x1 = 0, r_y1 = 0, r_x2 = 0, r_y2 = 0, count = 1;
};

void Palette(QPainter *painter);

#endif // RECT_H

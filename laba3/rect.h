#ifndef RECT_H
#define RECT_H

class QPainter;
class QPoint;

class Rect
{
public:
    Rect();
    void setCoords(int x1, int y1, int x2, int y2);
    void Draw(QPainter *painter);
    bool Setted();
    bool Contains(QPoint *point);
    void clear();

private:
    int r_x1 = 0, r_y1 = 0, r_x2 = 0, r_y2 = 0;
};

#endif // RECT_H

#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include <QMessageBox>
#include <QPoint>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;    
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    if (rect.Setted())
        rect.Draw(&painter);
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    static int x1 = 0, y1 = 0;
    if (!rect.Setted() && ((x1 && y1) == 0))
    {
        x1 = event->pos().x();
        y1 = event->pos().y();
    }
    else if (x1 && y1)
    {
        rect.setCoords(x1, y1, event->pos().x(), event->pos().y());
        x1 = 0;
        y1 = 0;
        update();
    }
}

void MainWindow::mouseDoubleClickEvent(QMouseEvent *event)
{
    QPoint point;
    point.setX(event->pos().x());
    point.setY(event->pos().y());
    QMessageBox msg;
    if (rect.Contains(&point))
        msg.setText("Got it!");
    else
        msg.setText("Missed");
    msg.exec();
    rect.clear();
}


#include "rect.h"
#include <QPainter>
#include <QBrush>
#include <QColor>

Rect::Rect()
{

}

void Rect::setCoords(int x1, int y1, int x2, int y2)
{
    if (x1 > x2)
        std::swap(x1, x2);
    if (y1 > y2)
        std::swap(y1, y2);
    r_x1 = x1;
    r_y1 = y1;
    r_x2 = x2;
    r_y2 = y2;
}

void Rect::Draw(QPainter *painter)
{
    painter->drawRect(r_x1, r_y1, r_x2 - r_x1, r_y2 - r_y1);
}

bool Rect::Setted()
{
    return r_x1 && r_y1 && r_x2 && r_y2;
}

bool Rect::Contains(QPoint *point)
{
    return ((r_x1 - point->x())*(r_x2 - point->x()) < 0)
            && ((r_y1 - point->y())*(r_y2 - point->y()) < 0);
}

void Rect::clear()
{
    r_x1 = 0;
    r_y1 = 0;
    r_x2 = 0;
    r_y2 = 0;
}

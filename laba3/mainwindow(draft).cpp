#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTimer>
#include <QPainter>
#include <QMessageBox>
#include <QPoint>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
//    connect(timer, &QTimer::timeout, this, &MainWindow::pointSetting(p2));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    if (rect.pointsSetted())
        rect.Draw(&painter);
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    if (timer.isActive())
    {
        timer.stop();
        qDebug("mouseDoubleClickEvent");
        QPoint point;
        point.setX(event->pos().x());
        point.setY(event->pos().y());
        qDebug("x = %d y = %d", event->pos().x(), event->pos().y());
        QMessageBox msg;
        if (rect.Contains(&point))
            msg.setText("Got it!");
        else
            msg.setText("Missed");
        msg.exec();
    }
    else
    {
//        static bool flag = 0;
//        timer.setSingleShot(true);
//        timer.start(200);
        qDebug("mousePressEvent");
            if (flag == 0)
            {
                qDebug("setPoint1");
//                rect = new Rect;
//                rect->setPoint1(event->pos().x(), event->pos().y());
                p1.setX(event->pos().x());
                p1.setY(event->pos().y());
                QTimer::singleShot(250, &rect, MainWindow::pointSetting(p1));
//                flag = 1;
            }
            else if (flag == 1)
            {
//                if (rect->FirstPointSetted())
//                {
                    qDebug("setPoint2");
//                    rect->setPoint2(event->pos().x(), event->pos().y());
//                    rect->SortCoords();
                    p2.setX(event->pos().x());
                    p2.setY(event->pos().y());
                    QTimer::singleShot(250, &rect, MainWindow::pointSetting(p2));
//                    update();
//                }
//                flag = 0;
            }
    }
}

void MainWindow::pointSetting(QPoint point)
{
    if (flag == 0)
    {
        rect.setPoint1(point.x(), point.y());
        flag = 1;
    }
    else if (flag == 1)
    {
        rect.setPoint2(point.x(), point.y());
        rect.SortCoords();
        flag = 0;
        update();
    }
}



//void MainWindow::mouseDoubleClickEvent(QMouseEvent *event)
//{
//    qDebug("mouseDoubleClickEvent");
//    QPoint point;
//    point.setX(event->pos().x());
//    point.setY(event->pos().y());
//    qDebug("x = %d y = %d", event->pos().x(), event->pos().y());
//    QMessageBox msg;
//    if (rect->Contains(&point))
//        msg.setText("Got it!");
//    else
//        msg.setText("Missed");
//    msg.exec();
//}


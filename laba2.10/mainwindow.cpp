#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMouseEvent>
#include <QPaintEvent>
#include <QPainter>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    if (!points.empty())
    {
            int x = points[0].getX();
            int y = points[0].getY();
        for (int i = 0; i < points.size(); ++i)
        {
            painter.drawLine(x, y, points[i].getX(), points[i].getY());
            points[i].drawCross(&painter);
            x = points[i].getX();
            y = points[i].getY();
        }
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    bool flag = true;
    Point p(event->pos().x(), event->pos().y());
        for (auto i = 0; i < points.size(); ++i)
            if (points[i].distance2(p))
            {
                data[0] = i;
                data[1] = event->pos().x() - points[i].getX();
                data[2] = event->pos().y() - points[i].getY();
                flag =false;
            }
        if (flag)
            points.push_front(p);
    update();
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    if (!points.empty() && data[0] != -1)
    {
        points[data[0]].setCoords(event->pos().x() - data[1], event->pos().y() - data[2]);
        update();
    }
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event)
{
    if (data[0] != -1)
    {
        data[0] = -1;
        data[1] = 0;
        data[2] = 0;
    }
}


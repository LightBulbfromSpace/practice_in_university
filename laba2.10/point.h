#ifndef POINT_H
#define POINT_H

class QPainter;

class Point
{
public:
    Point();
    Point(int x, int y);
    void setCoords(int x, int y);
    void drawCross(QPainter *painter);
    void setOffset (int offset);
    void setWidth(int width);
    bool distance2(const Point &p);
    int getX() const;
    int getY() const;

private:
    int p_x, p_y, p_offset = 2, p_width = 1;
};

#endif // POINT_H

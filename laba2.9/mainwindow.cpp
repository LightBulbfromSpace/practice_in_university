#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMouseEvent>
#include <QPaintEvent>
#include <QPainter>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    if (!points.empty())
    {
            int x = points[0].getX();
            int y = points[0].getY();
        for (int i = 0; i < points.size(); ++i)
        {
            painter.drawLine(x, y, points[i].getX(), points[i].getY());
            points[i].drawCross(&painter);
            x = points[i].getX();
            y = points[i].getY();
        }
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    Point p(event->pos().x(), event->pos().y());
    bool flag = true;
        for (auto i = 0; i < points.size(); ++i)
            if (points[i].distance2(p))
            {
                points.remove(i);
                flag = false;
            }
       if (flag)
            points.push_front(p);
    update();
}

